using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    public string playerName;
    public float speed;
    bool _jumpState;
    float _jumptime;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        speed = 6f;
        playerName = PlayerPrefs.GetString("name");
        _jumpState = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= -7) Die();
        transform.position += new Vector3(Input.GetAxis("Horizontal")*speed*Time.deltaTime, 0,0);
        if (Input.GetKeyDown("space")&&!_jumpState) { 
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 5, ForceMode2D.Impulse);
            _jumpState = true;
        }
        else if (_jumpState) {
            _jumptime += Time.deltaTime;
            if (_jumptime >= 1f)
            {
                _jumpState = false;
                _jumptime = 0;
            }
        }
        CheckOrientation();
        animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        animator.SetBool("jump", _jumpState);
    }
    void CheckOrientation()
    {
        if (Input.GetAxis("Horizontal") < 0)GetComponent<SpriteRenderer>().flipX = true;
        if (Input.GetAxis("Horizontal") > 0) GetComponent<SpriteRenderer>().flipX = false;
    }
    public void Die()
    {
        if (GameManager.instance.IsSurvival())
        {
            PlayerPrefs.SetString("resul", "Defeat");
        }
        else
        {
            PlayerPrefs.SetString("resul", "Victory");
        }
        SceneManager.LoadScene("GameOverScreen");
    }
}
