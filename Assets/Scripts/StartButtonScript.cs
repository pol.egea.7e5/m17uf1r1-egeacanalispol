using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButtonScript : MonoBehaviour
{
    TMPro.TMP_InputField _datajugador;
    // Start is called before the first frame update
    void Start()
    {
        _datajugador = GameObject.Find("PlayerNameInput")?.GetComponent<TMPro.TMP_InputField>();
        GetComponent<Button>().onClick.AddListener(taskOnClick);
    }

    // Update is called once per frame
    void taskOnClick()
    {
        if (_datajugador.text != "")
        {
            PlayerPrefs.SetString("name",_datajugador.text);
            PlayerPrefs.SetString("mode",gameObject.name);
            SceneManager.LoadScene("GameScreen");
        }
        else Debug.LogError("No s'ha posat nom al jugador");
    }
}
