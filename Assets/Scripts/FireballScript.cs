using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour
{
    bool _ground;
    Animator animator;
    float _speed=5f;
    // Update is called once per frame
    void Update()
    {
        _ground = false;
        animator = GetComponent<Animator>();
        transform.position += new Vector3(0, -(_speed * Time.deltaTime), 0);
        if (transform.position.y<=-5) animator.SetBool("explosion", true);
    }
    public void Explosion()
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_ground) {
            _ground = true;
            _speed = 0;
            animator.SetBool("explosion", true);
            if (collision.name == "Player") collision.GetComponent<PlayerScript>()?.Die(); }
    }
}
