using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayAgainButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(taskOnClick);
    }

    void taskOnClick()
    {
        SceneManager.LoadScene("GameScreen");
    }
}
