using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TMPro.TMP_Text text =GetComponent<TMPro.TMP_Text>();
        string resul=PlayerPrefs.GetString("resul");
        if (resul=="Victory")
        {
            text.color =Color.green;
        }
        else
        {
            text.color = Color.red;
        }
        text.text = resul;
    }
}
