using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public float timeLeft;
    public Object fireball;
    public string mode;
    public float timerspawn;
    TMPro.TMP_Text _timerText;
    // Start is called before the first frame update
    void Start()
    {
        timerspawn=0;
        _timerText = GameObject.Find("TimerText")?.GetComponent<TMPro.TMP_Text>();
        instance = this;
        timeLeft = 60.0f;
        _timerText.text = "60";
        mode = PlayerPrefs.GetString("mode");
    }

    // Update is called once per frame
    void Update()
    {
        if (IsSurvival())
        {
            timerspawn += Time.deltaTime;
            if (timerspawn >= 0.4)
            {
                GenerateFireball();
                timerspawn = 0;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Instantiate(fireball,new Vector3(Camera.main.ScreenPointToRay(Input.mousePosition).origin.x,7,0),new Quaternion(0,0,0,0));
            }
        }
        timeLeft -= Time.deltaTime;
        _timerText.text = Mathf.Round(timeLeft).ToString();
        if (timeLeft <= 0)
        {
            finaltimer();
        }
    }
    public bool IsSurvival()
    {
        if (mode == "SurvivalButton") return true;
        return false;
    }
    void finaltimer()
    {
        if (IsSurvival())
        {
            PlayerPrefs.SetString("resul", "Victory");
        }
        else
        {
            PlayerPrefs.SetString("resul", "Defeat");
        }
        SceneManager.LoadScene("GameOverScreen");
    }
    void GenerateFireball()
    {
        Instantiate(fireball,new Vector3(Random.Range(-10,10),7f,0f),new Quaternion(0,0,0,0));
    }
}
